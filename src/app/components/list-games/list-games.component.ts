import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-games',
  templateUrl: './list-games.component.html',
  styleUrls: ['./list-games.component.css']
})
export class ListGamesComponent implements OnInit {
  games=[];
  games_izbr=[];
  games_izbr_people=localStorage.getItem('izbr_games').split(',');
  games_izbr_people_mas=[];
  games_izbr_id=["1486275","1486278","1595791"];
  kategories=[];
  merchants=[];
  games_vivod=[];
  constructor() { }
  changeCat(kat_id:number){
    console.log(kat_id);
    this.games_vivod=[];
    if(kat_id==0){
      this.games_vivod=this.games_vivod.concat(this.games_izbr).concat(this.games_izbr_people_mas).concat(this.games);
    }
    if(kat_id!=-1){
      this.games_vivod=this.games_vivod.concat(this.games_izbr).concat(this.games_izbr_people_mas);
      for(var i=0;i<this.games.length;i++){
        if(this.games[i].CategoryID.indexOf(kat_id)!=-1){this.games_vivod.push(this.games[i]);}
      }
    }
    if(kat_id==-1){
      this.games_vivod=this.games_vivod.concat(this.games_izbr).concat(this.games_izbr_people_mas);
    }

    return this.games_vivod;
  }
  changeMerch(merch_id){
    return this.games_vivod;
  }

  izbr_func(gameID):boolean{
    if(this.games_izbr_people.indexOf(gameID) !=-1){return true}else{return false;}
  }
  ngOnInit(): void {
    var targetUrl = '/api/v1/games?lang=en'
    fetch(targetUrl,{
      method:'GET',
      mode: 'cors',
      headers: {
      'Content-Type': 'application/json; charset=utf-8'
      }})
      .then(blob => blob.json())
      .then(data => {
        this.kategories=data.categories;
        this.games=data.games;
        this.games.sort((prev, next) => {
            if ( prev.Name.en < next.Name.en ) return -1;
            if ( prev.Name.en > next.Name.en ) return 1;
        });
        for(var i=0;i<this.games_izbr_id.length;i++){
          for(var i1=0;i1<this.games.length;i1++){
            if(this.games_izbr_id[i]==this.games[i1].ID){
              this.games_izbr.push(this.games[i1]);
              this.games.splice(i1,1);
              break;
            }
          }
        }

        for(var i=0;i<this.games_izbr_people.length;i++){
          for(var i1=0;i1<this.games.length;i1++){
            if(this.games_izbr_people[i]==this.games[i1].ID){
              this.games_izbr_people_mas.push(this.games[i1]);
              this.games.splice(i1,1);
              break;
            }
          }
        }
        this.games_vivod=this.games_vivod.concat(this.games_izbr).concat(this.games_izbr_people_mas).concat(this.games);
        console.table(data);
        return data;
      })
      .catch(e => {
        console.log(e);
        return e;
      });
  }
  reCount(){}
  onChanged(increased:any){
        console.log('Старые избранные - '+localStorage.getItem('izbr_games'));
        var list=localStorage.getItem('izbr_games').split(',');
        var otvet='';
        if(list.indexOf(increased[1]) != -1){
          list.splice(list.indexOf(increased[1]),1);
          console.log('удалили');
        }else{
          console.log('добавили');
          list.push(increased[1]);
        }
        list.forEach(function(item, i, arr) {
          if(item){
              otvet+=item+',';//  alert( i + ": " + item + " (массив:" + arr + ")" );
          }
        });

        console.log(increased[0]+' '+increased[1]);
        localStorage.setItem('izbr_games', otvet);
        console.log('Новые избранные - '+otvet);
       //increased==true?this.clicks++:this.clicks--;
   }
}
