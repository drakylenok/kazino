import { Component, OnInit ,EventEmitter , Input, Output} from '@angular/core';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {

  @Input() gameIzbr:boolean;
  @Input() gameName: string;
  @Input() gameImg: string;
  @Input() gameId: number;
  @Output() onChanged = new EventEmitter<any>();


  change(inc:any) {
      this.gameIzbr=!this.gameIzbr;
      this.onChanged.emit([this.gameIzbr, this.gameId]);
  }
  constructor() { }

  ngOnInit(): void {}

}
